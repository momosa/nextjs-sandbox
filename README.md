# Next.js sandbox

Messing around with Next.js.

This project uses the App Router (rather than the Page Router)
and Typescript.
The project base was set up using
[this guide][nextjs_quick_start].

## Setup

```shell
npm install
```

## Dev server

Run the server locally:

```shell
npm run dev
```

## Pages

### Map

This feature requires setting `NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN` in `.env.local`,
which is not tracked by git.
For example:

```shell
# .env.local
NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN=<Your mapbox access token>
```

The `NEXT_PUBLIC_` prefix is required.
See [this documentation][nextjs_env_vars].
The prefix includes the access token in the build,
so the browser has it and can get the map.
*WARNING: this makes the token accessible to the world if you publish site!*

You can create an access token using a free account at
[mapbox.com][mapbox].

Url path on a local dev server:

```
localhost:3000/map/
```

Corresponding file locations:

```shell
# Map component usage
src/app/map/

# Map PNG generation and download functionality
src/utils/map-png.ts
```

Used this tutorial to set up the map:
[How to use Mapbox in Next js][mapbox_tutorial].
There's some additional stuff about custom markers in there,
but I didn't include that.


[mapbox]: https://mapbox.com/
[mapbox_tutorial]: https://recodebrain.com/how-to-use-mapbox-in-next-js/
[nextjs_quick_start]: https://nextjs.org/docs/getting-started/installation/
[nextjs_env_vars]: https://nextjs.org/docs/pages/building-your-application/configuring/environment-variables/
