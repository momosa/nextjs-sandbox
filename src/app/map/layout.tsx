/* Layout: Map Page
*******************************************************************************/
"use client"

import { MapProvider } from "react-map-gl"

/******************************************************************************/

export default function MapLayout({
  children,
}: {
  children: React.ReactNode,
}) {
  return (
    <MapProvider>
      {children}
    </MapProvider>
  )
}
