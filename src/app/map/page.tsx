/* Map Page
*******************************************************************************/
"use client"

// Can also import Map as default
//import Map, { NavigationControl, GeolocateControl } from "react-map-gl"

import {
  Map,
  useMap,
  NavigationControl,
  GeolocateControl,
} from "react-map-gl"
import "mapbox-gl/dist/mapbox-gl.css"

import type { MapRef } from "react-map-gl"

import { downloadMapPNG } from "@local/src/utils/map-png"

import styles from "./page.module.css"

/******************************************************************************/

export default function MapPage() {
  const mapboxToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN

  const map = useMap()["MyMap"]
  const downloadPNG = () => downloadMapPNG(map, "map.png")

  return (
    <main className={styles.mainStyle}>
      <button onClick={downloadPNG}>
        Download PNG
      </button>
      <Map
        id="MyMap"
        mapboxAccessToken={mapboxToken}
        mapStyle="mapbox://styles/mapbox/streets-v12"
        style={styles.mapStyle}
        initialViewState={{ latitude: 42.3314, longitude: -83.0458, zoom: 10 }}
        maxZoom={20}
        minZoom={3}
        preserveDrawingBuffer={true}
      >
        <GeolocateControl position="top-left" />
        <NavigationControl position="top-left" />
      </Map>
    </main>
  )
}

