/* map-png.ts
 *
 * getMapPNG(map: MapRef): string
 * Get PNG data for a given map
 *
 * downloadMapPNG(map: MapRef): void
 * Download a map PNG
 *
*******************************************************************************/

import type { MapRef } from "react-map-gl"

/******************************************************************************/

/* getMapPNG
 *
 * Get PNG data for a given map
 *
 * Params:
 *   map: MapRef: the map from which to make the PNG
 *
 * Returns:
 *   string: the PNG data
 *
 * Notes:
 *   - If using react-map-gl, get the map with useMap within a MapProvider context
 *   - The map must have preserveDrawingBuffer set to true on creation
 */
function getMapPNG(
  map: MapRef,
): string {
  console.assert(!!map)
  const pngData = map.getCanvas().toDataURL("image/png")
  return pngData
}

/******************************************************************************/

/* downloadMapPNG
 *
 * Download a given map as the given filename
 *
 * Params:
 *   map: MapRef: the map to download
 *   downloadFilename: string: the name of the png to download, like "my_map.png"
 *
 * Returns:
 *   void
 *
 * Notes:
 *   - If using react-map-gl, get the map with useMap within a MapProvider context
 *   - The map must have preserveDrawingBuffer set to true on creation
 */
function downloadMapPNG (
  map: MapRef,
  downloadFilename: string,
): void {
  const pngData = getMapPNG(map)
  let download = document.createElement("a")
  download.href = pngData.replace("image/png", "image/octet-stream")
  download.download = downloadFilename
  download.click()
}


/******************************************************************************/

export {
  getMapPNG,
  downloadMapPNG,
}
